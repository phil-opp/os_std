use core::cell::{Ref, RefMut, RefCell};
use core::intrinsics;
use core::mem;

use sys_common::thread_info;

#[deriving(Copy)]
pub struct Slot<T> {
    number: SlotNumber,
}

enum SlotNumber {
    ThreadInfo = 1,
    StackLimit = 112/8,
}

pub const THREAD_INFO_SLOT: Slot<thread_info::ThreadInfo> = Slot {
    number: SlotNumber::ThreadInfo,
};

impl<T> Slot<T> {
    pub unsafe fn borrow(self) -> Ref<'static, T> {
        (*self.slot_ptr()).borrow()
    }

    pub unsafe fn borrow_mut(self) -> RefMut<'static, T> {
        (*self.slot_ptr()).borrow_mut()
    }

    pub unsafe fn set(self, value: &'static mut RefCell<T>) {
        *(self.slot_ptr() as *mut &'static mut RefCell<T>) = value;
    }

    /// returns a pointer to the slot value (which is a static borow pointer)
    unsafe fn slot_ptr(self) -> *const (&'static RefCell<T>) {
        let mut fs_addr : *const (&'static RefCell<T>);
        asm!("movq %fs, $0" : "=r"(fs_addr) ::: "volatile")
        intrinsics::offset(fs_addr, self.number as int)
    }
}


