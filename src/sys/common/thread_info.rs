// Copyright 2014 The Rust Project Developers. See the COPYRIGHT
// file at the top-level directory of this distribution and at
// http://rust-lang.org/COPYRIGHT.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.


use core::prelude::*;

use thread::Thread;
use cell::RefCell;
use string::String;

use tls;

pub struct ThreadInfo {
    // This field holds the known bounds of the stack in (lo, hi)
    // form. Not all threads necessarily know their precise bounds,
    // hence this is optional.
    stack_bounds: (uint, uint),
    stack_guard: uint,
    unwinding: bool,
    thread: Thread,
}

static THREAD_INFO: tls::Slot<ThreadInfo> = tls::THREAD_INFO_SLOT;

pub fn current_thread() -> Thread {
    unsafe{THREAD_INFO.borrow()}.thread.clone()
}

pub fn panicking() -> bool {
    unsafe{THREAD_INFO.borrow()}.unwinding
}

pub fn stack_guard() -> uint {
    unsafe{THREAD_INFO.borrow()}.stack_guard
}

pub fn set_unwinding(unwinding: bool) {
    unsafe{THREAD_INFO.borrow_mut()}.unwinding = unwinding
}

pub fn set(stack_bounds: (uint, uint), stack_guard: uint, thread: Thread) {
    *unsafe{THREAD_INFO.borrow_mut()} = ThreadInfo {
        stack_bounds: stack_bounds,
        stack_guard: stack_guard,
        unwinding: false,
        thread: thread,
    }
}
